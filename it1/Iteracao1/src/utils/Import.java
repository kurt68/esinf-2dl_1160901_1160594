/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import model.Bilhete;
import model.Estacao;

public class Import {

    public static DoublyLinkedList<Estacao> lerEstacoes() throws FileNotFoundException { //guarda as Estacoes numa DoublyLinkedList
        Scanner ler = new Scanner(new File("fx_estacoes.txt"));
        DoublyLinkedList<Estacao> estacoes = new DoublyLinkedList<>();
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            String[] aux = linha.trim().split(",");
            Estacao e = new Estacao(Integer.parseInt(aux[0]), aux[1], aux[2]);
            estacoes.sortingInsert(e);
        }
        ler.close();
        return estacoes;
    }

    public static List<Bilhete> lerViagens() throws FileNotFoundException { // guarda os bilhetes(com Estacao associada) numa ArrayList
        Scanner ler = new Scanner(new File("fx_viagens.txt"));
        List<Bilhete> bilhetes = new ArrayList<>();
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            String[] aux = linha.trim().split(",");
            Bilhete b = new Bilhete(Integer.parseInt(aux[0]), aux[1], getEstacaoByID(Integer.parseInt(aux[2]),lerEstacoes()), getEstacaoByID(Integer.parseInt(aux[3]),lerEstacoes()));
            bilhetes.add(b);
        }
        ler.close();
        return bilhetes;
    }

    public static Estacao getEstacaoByID(int id, DoublyLinkedList<Estacao> estacoes) { // retorna uma estacao atraves do seu id
        for (Estacao e : estacoes) {
            if (e.getId() == id) {
                return e;
            }
        }
        return null;
    }
}
