package model;

import java.util.Objects;

public class Bilhete {

    private int num;
    private String tipo;
    private Estacao origem;
    private Estacao destino;

    private static final int NUM_OMISSAO = 0;
    private static final String TIPO_OMISSAO = "SEM TIPO";
    private static final Estacao ORIGEM_OMISSAO = new Estacao();
    private static final Estacao DESTINO_OMISSAO = new Estacao();

    public Bilhete(int num, String tipo, Estacao origem, Estacao destino) {
        this.num = num;
        this.tipo = tipo;
        this.origem = origem;
        this.destino = destino;
    }    
    public Bilhete(Bilhete b) {
        this.num = b.num;
        this.tipo = b.tipo;
        this.origem = b.origem;
        this.destino = b.destino;
    }

    public Bilhete() {
        num = NUM_OMISSAO;
        tipo = TIPO_OMISSAO;
        origem = ORIGEM_OMISSAO;
        destino = DESTINO_OMISSAO;
    }

    public int getNum() {
        return num;
    }

    public String getTipo() {
        return tipo;
    }

    public Estacao getOrigem() {
        return origem;
    }

    public Estacao getDestino() {
        return destino;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public void setOrigem(Estacao origem) {
        this.origem = origem;
    }

    public void setDestino(Estacao destino) {
        this.destino = destino;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.num);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Bilhete other = (Bilhete) obj;
        if (this.num != other.num) {
            return false;
        }
        if (!Objects.equals(this.tipo, other.tipo)) {
            return false;
        }
        if (!Objects.equals(this.origem, other.origem)) {
            return false;
        }
        if (!Objects.equals(this.destino, other.destino)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Bilhete{" + "num=" + num + ", tipo=" + tipo + ", origem=" + origem.getDescricao() + ", destino=" + destino.getDescricao() + '}';
    }
}
