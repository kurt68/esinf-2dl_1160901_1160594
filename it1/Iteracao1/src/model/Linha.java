/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import utils.DoublyLinkedList;
import utils.Import;

/**
 *
 * @author Dell M-4700
 */
public class Linha {

    public DoublyLinkedList<Estacao> listaEstacoes;
    public List<Bilhete> listaBilhetes;

    public Linha() {
        this.listaEstacoes = new DoublyLinkedList<>();
        this.listaBilhetes = new ArrayList<>();
    }

    public void getData() throws FileNotFoundException {

        this.listaEstacoes = Import.lerEstacoes();
        this.listaBilhetes = Import.lerViagens();
    }

    public void adicionarBilhetesValidados(DoublyLinkedList<Estacao> listaEstacoes, List<Bilhete> listaBilhetes) {
        for (Bilhete b : listaBilhetes) {
            for (Estacao e : listaEstacoes) {
                if (b.getOrigem().equals(e)) {
                    e.setListaBilhetesValidadosEntrada(b);
                } else if (b.getDestino().equals(e)) {
                    e.setListaBilhetesValidadosSaida(b);
                }
            }
        }
    }

    //Exercicio2
    public Map<Estacao, Integer> calcularUtilizadoresPorEstacao(DoublyLinkedList<Estacao> listaEstacoes, List<Bilhete> listaBilhetes) {
        Map<Estacao, Integer> map = new TreeMap<>();
        int cont = 0, numEstacao;
        Estacao estacao;
        for (Estacao e : listaEstacoes) {
            map.put(e, 0);
        }
        for (Bilhete b : listaBilhetes) {
            Estacao origem = b.getOrigem();
            Estacao destino = b.getDestino();
            if (origem.getId() > destino.getId()) {
                Estacao aux = origem;
                origem = destino;
                destino = aux;
            }
            numEstacao = origem.getId();
            while (numEstacao != destino.getId()) {
                estacao = Import.getEstacaoByID(numEstacao, listaEstacoes);
                if(estacao==null) {
                    numEstacao++;
                    continue;
                }
                cont = map.get(estacao);
                cont++;
                map.put(estacao, cont);
                numEstacao++;
            }
            if (numEstacao == destino.getId()) {
                estacao = Import.getEstacaoByID(numEstacao, listaEstacoes);
                cont = map.get(estacao);
                cont++;
                map.put(estacao, cont);
            }
        }
        return map;
    }

    //Exercicio 3
    public List<LinkedList<Estacao>> maiorSequenciaDeEstacoesComUmTipo(DoublyLinkedList<Estacao> listaEstacoes, Bilhete bilhete) throws CloneNotSupportedException {
        if (bilhete.getTipo().isEmpty() || bilhete.getTipo().length() != 2) {
            return null;
        }
        DoublyLinkedList<Estacao> nova = (DoublyLinkedList) listaEstacoes.clone();
        List<LinkedList<Estacao>> sequencias = new LinkedList<>();
        List<Estacao> sequencia = new LinkedList<>();
        List<Estacao> aux = new LinkedList<>();
        int numZonasPossiveis = Integer.parseInt(bilhete.getTipo().substring(1));
        List<String> zonas = new LinkedList<>();
        Estacao estacao;
        while (nova.listIterator().hasNext()) {
            estacao = nova.listIterator().next();
            aux.add(estacao);
            zonas.add(estacao.getZona());
            for (Estacao e : nova) {
                if (e.equals(estacao)) {
                    continue;
                }
                if (numZonasPossiveis >= zonas.size()) {
                    if (zonas.contains(e.getZona())) {
                        aux.add(e);
                    } else if (numZonasPossiveis != zonas.size()) {
                        zonas.add(e.getZona());
                        aux.add(e);
                    } else {
                        break;
                    }
                }
            }
            if (aux.size() > sequencia.size()) {
                sequencias.clear();
                sequencia.clear();
                sequencia.addAll(aux);
                sequencias.add((LinkedList<Estacao>) sequencia);
            } else if (aux.size() == sequencia.size()) {
                sequencias.add((LinkedList<Estacao>) sequencia);
                sequencias.add((LinkedList<Estacao>) aux);
            }
            aux.clear();
            zonas.clear();
            nova.removeFirst();
        }
        return sequencias;
    }

    //Exercicio 4
    public Set<Bilhete> bilhetesTransgressao(DoublyLinkedList<Estacao> listaEstacoes, List<Bilhete> listaBilhetes) {
        Set<Bilhete> bilhetesTransgressao = new HashSet<>();
        Set<String> zonas = new HashSet<>();
        int numEstacao;
        Estacao estacao;
        for (Bilhete b : listaBilhetes) {
            Estacao origem = b.getOrigem();
            Estacao destino = b.getDestino();
            if (origem.getId() > destino.getId()) {
                Estacao aux = origem;
                origem = destino;
                destino = aux;
            }
            numEstacao = origem.getId();
            while (numEstacao != destino.getId()) {
                estacao = Import.getEstacaoByID(numEstacao, listaEstacoes);
                if(estacao==null) {
                    numEstacao++;
                    continue;
                }
                zonas.add(estacao.getZona());
                numEstacao++;
            }
            if (numEstacao == destino.getId()) {
                estacao = Import.getEstacaoByID(numEstacao, listaEstacoes);
                zonas.add(estacao.getZona());
            }
            int numZonasPossiveis = Integer.parseInt(b.getTipo().substring(1));
            if (numZonasPossiveis < zonas.size()) {
                bilhetesTransgressao.add(b);
            }
            zonas.clear();
        }
        return bilhetesTransgressao;
    }
}
