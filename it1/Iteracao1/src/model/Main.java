/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author Dell M-4700
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     * @throws java.lang.CloneNotSupportedException
     */
    public static void main(String[] args) throws FileNotFoundException, CloneNotSupportedException {
        // TODO code application logic here

        Linha l = new Linha();
        l.getData();
        Bilhete b = new Bilhete(1,"Z2",new Estacao(1,"Ex","C5"),new Estacao(1,"Es","C4"));
        l.adicionarBilhetesValidados(l.listaEstacoes,l.listaBilhetes);
        Map<Estacao, Integer> map = l.calcularUtilizadoresPorEstacao(l.listaEstacoes,l.listaBilhetes);
        Map<String, Integer> m = new TreeMap<>();
        for(Estacao e : map.keySet()) {
            m.put(e.getDescricao(), map.get(e));
        }
        System.out.println("---------mapa com utilizadores-------------------");
        System.out.println(map);
//        System.out.println(m);
        
        System.out.println("-----------sequencia para Z2-----------");
        System.out.println(l.maiorSequenciaDeEstacoesComUmTipo(l.listaEstacoes, b));
        
        System.out.println("-----------------bilhetes Transgressao---------");
        System.out.println(l.bilhetesTransgressao(l.listaEstacoes, l.listaBilhetes));
    }
}
