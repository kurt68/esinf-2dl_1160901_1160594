package model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Dell M-4700
 */
public class Estacao implements Comparable {

    private int id;
    private String descricao;
    private String zona;
    private Set<Bilhete> listaBilhetesValidadosEntrada;
    private Set<Bilhete> listaBilhetesValidadosSaida;

    private static final int ID_DEFAULT = 00;
    private static final String DESCRICAO_DEFAULT = "Estacao X";
    private static final String ZONA_DEFAULT = "Z0";

    public Estacao(int id, String descricao, String zona) {
        this.id = id;
        this.descricao = descricao;
        this.zona = zona;
        this.listaBilhetesValidadosEntrada = new HashSet<>();
        this.listaBilhetesValidadosSaida = new HashSet<>();
    }

    public Estacao() {
        this.id = ID_DEFAULT;
        this.descricao = DESCRICAO_DEFAULT;
        this.zona = ZONA_DEFAULT;
        this.listaBilhetesValidadosEntrada = new HashSet<>();
        this.listaBilhetesValidadosSaida = new HashSet<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public Set<Bilhete> getListaBilhetesValidadosEntrada() {
        return listaBilhetesValidadosEntrada;
    }

    public void setListaBilhetesValidadosEntrada(Bilhete b) {
        this.listaBilhetesValidadosEntrada.add(new Bilhete(b));
    }

    public Set<Bilhete> getListaBilhetesValidadosSaida() {
        return listaBilhetesValidadosSaida;
    }

    public void setListaBilhetesValidadosSaida(Bilhete b) {
        this.listaBilhetesValidadosSaida.add(new Bilhete(b));
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estacao other = (Estacao) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.descricao, other.descricao)) {
            return false;
        }
        if (!Objects.equals(this.zona, other.zona)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estacao{" + "id=" + id + ", descricao=" + descricao + ", zona=" + zona + '}';

    }

    @Override
    public int compareTo(Object o) {
        return this.id - ((Estacao) o).id;
    }
}
