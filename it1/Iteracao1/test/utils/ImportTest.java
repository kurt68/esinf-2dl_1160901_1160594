/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import model.Bilhete;
import model.Estacao;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dell M-4700
 */
public class ImportTest {
    
    public ImportTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of lerEstacoes method, of class Import.
     * @throws java.io.FileNotFoundException
     */
    
//    @Test
//    public void testLerEstacoes() throws FileNotFoundException {
//        System.out.println("lerEstacoes");
//        Estacao e1 = new Estacao(1,"Hospital de S.Joao","C1");
//        Estacao e2 = new Estacao(2,"IPO","C1");
//        Estacao e3 = new Estacao(3,"Marques","C1");
//        Estacao e4 = new Estacao(4,"Almeida da Silva", "C2");
//        Estacao e5 = new Estacao(5,"Aliados","C2");
//        Estacao e6 = new Estacao(6,"Combatentes","C2");
//        Estacao e7 = new Estacao(7,"Trindade","C3");
//        Estacao e8 = new Estacao(8,"Isep","C3");
//        Estacao e9 = new Estacao(9,"Bolhao","C3");
//        Estacao e10 = new Estacao(10,"Casa da Musica","C4");
//        DoublyLinkedList<Estacao> expResult = new DoublyLinkedList<>();
//        expResult.addLast(e1);
//        expResult.addLast(e2);
//        expResult.addLast(e3);
//        expResult.addLast(e4);
//        expResult.addLast(e5);
//        expResult.addLast(e6);
//        expResult.addLast(e7);
//        expResult.addLast(e8);
//        expResult.addLast(e9);
//        expResult.addLast(e10);
//        DoublyLinkedList<Estacao> result = Import.lerEstacoes();
//        assertEquals(expResult, result);
//    }
//
//    /**
//     * Test of lerViagens method, of class Import.
//     * @throws java.io.FileNotFoundException
//     */
//    @Test
//    public void testLerViagens() throws FileNotFoundException {
//        System.out.println("lerViagens");
//        Estacao e1 = new Estacao(1,"Hospital de S.Joao","C1");
//        Estacao e2 = new Estacao(2,"IPO","C1");
//        Estacao e3 = new Estacao(3,"Marques","C1");
//        Estacao e4 = new Estacao(4,"Almeida da Silva", "C2");
//        Estacao e5 = new Estacao(5,"Aliados","C2");
//        Estacao e6 = new Estacao(6,"Combatentes","C2");
//        Estacao e7 = new Estacao(7,"Trindade","C3");
//        Estacao e8 = new Estacao(8,"Isep","C3");
//        Estacao e9 = new Estacao(9,"Bolhao","C3");
//        Estacao e10 = new Estacao(10,"Casa da Musica","C4");
//        Bilhete b1 = new Bilhete(111222333,"Z2",e1,e10);
//        Bilhete b2 = new Bilhete(111333222,"Z3",e1,e2);
//        Bilhete b3 = new Bilhete(111444222,"Z2",e2,e1);
//        Bilhete b4 = new Bilhete(111555444,"Z3",e1,e7);
//        Bilhete b5 = new Bilhete(111666222,"Z2",e3,e4);
//        Bilhete b6 = new Bilhete(111777666,"Z2",e9,e2);
//        Bilhete b7 = new Bilhete(111999111,"Z3",e1,e10);
//        Bilhete b8 = new Bilhete(111888777,"Z4",e5,e10);
//        Bilhete b9 = new Bilhete(111999333,"Z2",e6,e8);
//        Bilhete b10 = new Bilhete(111777333,"Z2",e10,e3);
//        List<Bilhete> expResult = new ArrayList<>();
//        expResult.add(b1);
//        expResult.add(b2);
//        expResult.add(b3);
//        expResult.add(b4);
//        expResult.add(b5);
//        expResult.add(b6);
//        expResult.add(b7);
//        expResult.add(b8);
//        expResult.add(b9);
//        expResult.add(b10);
//        List<Bilhete> result = Import.lerViagens();
//        assertEquals(expResult, result);
//    }

    /**
     * Test of getEstacaoByID method, of class Import.
     * @throws java.io.FileNotFoundException
     */
    @Test
    public void testGetEstacaoByID() throws FileNotFoundException {
        System.out.println("getEstacaoByID");
        int id = 2;
        DoublyLinkedList<Estacao> lista = new DoublyLinkedList<>();
        Estacao expResult = new Estacao(2,"IPO","C1");
        Estacao e8 = new Estacao(8,"Isep","C3");
        Estacao e9 = new Estacao(9,"Bolhao","C3");
        lista.addLast(expResult);
        lista.addLast(e9);
        lista.addLast(e8);
        Estacao result = Import.getEstacaoByID(id, lista);
        assertEquals(expResult, result);
    }
}
