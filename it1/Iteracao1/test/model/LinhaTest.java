/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dell M-4700
 */
public class LinhaTest {

    private final Linha instance;
    Estacao e1 = new Estacao(1, "E1", "C1");
    Estacao e2 = new Estacao(2, "E2", "C3");
    Estacao e3 = new Estacao(3, "E3", "C1");
    Estacao e4 = new Estacao(4, "E4", "C2");
    Estacao e5 = new Estacao(5, "E5", "C2");
    Estacao e6 = new Estacao(6, "E6", "C1");
    Estacao e7 = new Estacao(7, "E7", "C2");
    Estacao e8 = new Estacao(8, "E8", "C3");
    Estacao e9 = new Estacao(9, "E9", "C4");
    Estacao e10 = new Estacao(10, "E10", "C5");
    Bilhete b1 = new Bilhete(1, "Z2", e1, e3);
    Bilhete b2 = new Bilhete(2, "Z2", e1, e2);
    Bilhete b3 = new Bilhete(3, "Z3", e1, e5);
    Bilhete b5 = new Bilhete(5, "Z2", e1, e5);
    Bilhete b10 = new Bilhete(10,"Z2",e2,e4);
    Bilhete b20 = new Bilhete(20,"Z2",e8,e10);
    Bilhete b25 = new Bilhete(25,"Z2",e9,e7);
    Bilhete b30 = new Bilhete(30,"Z3",e10,e7);
    Bilhete b51 = new Bilhete(51, "Z4", e1, e8);

    public LinhaTest() throws FileNotFoundException {
        instance = new Linha();
        instance.getData();
    }

    /**
     * Test of adicionarBilhetesValidados method, of class Linha.
     */
    @Test
    public void testAdicionarBilhetesValidados() { //utilizei a listaBilheteValidadosEntrada da estacao 1 para testar
        System.out.println("adicionarBilhetesValidados");
        instance.adicionarBilhetesValidados(instance.listaEstacoes, instance.listaBilhetes);
        Set<Bilhete> result = instance.listaEstacoes.iterator().next().getListaBilhetesValidadosEntrada();
        Set<Bilhete> expResult = new HashSet<>();
        expResult.add(b1);
        expResult.add(b2);
        expResult.add(b3);
        expResult.add(b5);
        expResult.add(b51);
        assertEquals(expResult, result);
    }

    /**
     * Test of calcularUtilizadoresPorEstacao method (Ex2), of class Linha.
     */
    @Test
    public void testCalcularUtilizadoresPorEstacao() {
        System.out.println("calcularUtilizadoresPorEstacao");
        Map<Estacao, Integer> result = instance.calcularUtilizadoresPorEstacao(instance.listaEstacoes, instance.listaBilhetes);
        Map<Estacao, Integer> expResult = new TreeMap<>();
        expResult.put(e1, 6);
        expResult.put(e2, 13);
        expResult.put(e3, 13);
        expResult.put(e4, 13);
        expResult.put(e5, 12);
        expResult.put(e6, 8);
        expResult.put(e7, 9);
        expResult.put(e8, 11);
        expResult.put(e9, 5);
        expResult.put(e10, 3);
        assertEquals(expResult, result);
    }

    /**
     * Test of maiorSequenciaDeEstacoesComUmTipo (Ex3), of class Linha.
     * @throws java.lang.CloneNotSupportedException
     */
    @Test
    public void testMaiorSequenciaDeEstacoesComUmTipo() throws CloneNotSupportedException {
        System.out.println("maiorSequenciaDeEstacoesComUmTipo");
        List<LinkedList<Estacao>> expResultZ2 = new LinkedList<>();
        List<LinkedList<Estacao>> resultZ2 = instance.maiorSequenciaDeEstacoesComUmTipo(instance.listaEstacoes, b1);  //teste para um bilhete Z2
        List<Estacao> sequenciaBilheteZ2 = new LinkedList<>();
        sequenciaBilheteZ2.add(e3);
        sequenciaBilheteZ2.add(e4);
        sequenciaBilheteZ2.add(e5);
        sequenciaBilheteZ2.add(e6);
        sequenciaBilheteZ2.add(e7);
        expResultZ2.add((LinkedList<Estacao>) sequenciaBilheteZ2);
        assertEquals(expResultZ2, resultZ2);
        List<LinkedList<Estacao>> expResultZ3 = new LinkedList<>();
        List<LinkedList<Estacao>> resultZ3 = instance.maiorSequenciaDeEstacoesComUmTipo(instance.listaEstacoes, b3);  //teste para um bilhete Z3
        List<Estacao> sequenciaBilheteZ3 = new LinkedList<>();
        sequenciaBilheteZ3.add(e1);
        sequenciaBilheteZ3.add(e2);
        sequenciaBilheteZ3.add(e3);
        sequenciaBilheteZ3.add(e4);
        sequenciaBilheteZ3.add(e5);
        sequenciaBilheteZ3.add(e6);
        sequenciaBilheteZ3.add(e7);
        sequenciaBilheteZ3.add(e8);
        expResultZ3.add((LinkedList<Estacao>) sequenciaBilheteZ3);
        assertEquals(expResultZ3, resultZ3);
        List<LinkedList<Estacao>> expResultZ4 = new LinkedList<>();
        List<LinkedList<Estacao>> resultZ4 = instance.maiorSequenciaDeEstacoesComUmTipo(instance.listaEstacoes, b51);  //teste para um bilhete Z4
        List<Estacao> sequenciaBilheteZ4 = new LinkedList<>();
        sequenciaBilheteZ4.add(e1);
        sequenciaBilheteZ4.add(e2);
        sequenciaBilheteZ4.add(e3);
        sequenciaBilheteZ4.add(e4);
        sequenciaBilheteZ4.add(e5);
        sequenciaBilheteZ4.add(e6);
        sequenciaBilheteZ4.add(e7);
        sequenciaBilheteZ4.add(e8);
        sequenciaBilheteZ4.add(e9);
        expResultZ4.add((LinkedList<Estacao>) sequenciaBilheteZ4);
        assertEquals(expResultZ4, resultZ4);
    }
    
     /**
     * Test of bilhetesTransgressao (Ex4), of class Linha.
     */
    @Test
    public void bilhetesTransgressao() {
        System.out.println("bilhetesTransgressao");
        Set<Bilhete> result = instance.bilhetesTransgressao(instance.listaEstacoes, instance.listaBilhetes);
        Set<Bilhete> expResult = new HashSet<>();
        expResult.add(b5);
        expResult.add(b10);
        expResult.add(b20);
        expResult.add(b25);
        expResult.add(b30);
        assertEquals(expResult,result);
    }
}
