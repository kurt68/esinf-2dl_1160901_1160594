/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Dell M-4700
 */
public class ControllerTest {

    Controller c;

    public ControllerTest() {
        c = new Controller();
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of descodificarPalavrasMorse method, of class Controller, ex2.
     */
    @Test
    public void testDescodificarPalavrasMorse() {
        System.out.println("descodificarPalavrasMorse");
        String palavra = ". _";
        String expResult = "ET";
        String result = c.descodificarPalavrasMorse(palavra);
        assertEquals(expResult, result);
    }

    /**
     * Test of criarArvoreLetras method, of class Controller, ex3.
     */
    @Test
    public void testCriarArvoreLetras() {
        System.out.println("criarArvoreLetras");
        Morse expResult = new Morse("....", "H", "Letter");
        TreeMorse result = c.criarArvoreLetras();
        assertEquals(expResult, result.smallestElement());
    }

    /**
     * Test of descodificarPalavra method, of class Controller, ex4.
     */
    @Test
    public void testDescodificarPalavra() {
        System.out.println("descodificarPalavra");
        String palavra = "ET";
        String expResult = ". _";
        String result = c.descodificarPalavra(palavra);
        assertEquals(expResult, result);
    }

    /**
     * Test of sequenciaComum method, of class Controller, ex5.
     */
    @Test
    public void testSequenciaComum() {
        System.out.println("sequenciaComum");
        String s1 = "5";
        String s2 = "4";
        String result = c.sequenciaComum(s1, s2);
        String expResult = "....";
        assertEquals(expResult, result);
    }

    /**
     * Test of filtrarEOrdenar method, of class Controller, ex6.
     */
    @Test
    public void testFiltrarEOrdenar() {
        System.out.println("filtrarEOrdenar");
        List<String> codigosMorse = new ArrayList<>();
        codigosMorse.add("._ ..___ ...__");
        codigosMorse.add("._ _... ...__");
        Comparator comp = (Comparator<Integer>) (Integer o1, Integer o2) -> o2.compareTo(o1);
        Map<Integer, String> expResult = new TreeMap<>(comp);
        expResult.put(2, "AB3");
        expResult.put(1, "A23");
        Map<Integer, String> result = c.getArvoreCompleta().filtrarEOrdenar(codigosMorse, "Letter");
        assertEquals(expResult, result);
    }
}
