/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import main.Morse;
import main.TreeMorse;

/**
 *
 * @author Joao Areias
 */
public class Import {

    public static TreeMorse lerArvore() throws FileNotFoundException {
        Scanner ler = new Scanner(new File("morse_v3.csv"));
        TreeMorse arvore = new TreeMorse();
        Morse start = new Morse("r", "start", "root");
        arvore.insert(start);
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            String[] aux = linha.trim().split(" ");
            Morse m = new Morse(aux[0], aux[1], aux[2]);
            arvore.insert(m);
        }
        ler.close();
        return arvore;
    }
}
