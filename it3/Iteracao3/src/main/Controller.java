/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.FileNotFoundException;
import java.util.List;
import java.util.Map;
import utils.Import;

/**
 *
 * @author Dell M-4700
 */
public class Controller {

    private TreeMorse arvore;

    public Controller() {
        try {
            criarArvore();
        } catch (FileNotFoundException ex) {
            System.out.println("Erro ao criar a arvore. " + ex.getMessage());
        }
    }

    //ex1 criar Arvore a partir do ficheiro 
    private void criarArvore() throws FileNotFoundException {
        this.arvore = Import.lerArvore();
    }

    public TreeMorse getArvoreCompleta() {
        return arvore;
    }

    //ex2 descodificar codigo morse para simbolo
    public String descodificarCodigoMorse(String codigoMorse) {
        return this.arvore.descodificarMorse(codigoMorse);
    }

    //ex2 descodificar palavras de codigos morse separadas por " "
    public String descodificarPalavrasMorse(String palavra) {
        return this.arvore.descodificarPalavrasMorse(palavra);
    }

    //ex3 criar nova Arvore apenas com letras
    public TreeMorse criarArvoreLetras() {
        return this.arvore.criarArvoreLetras("Letter");
    }

    //ex4 descodificar palavras para codigo morse
    public String descodificarPalavra(String palavra) {
        TreeMorse letras = criarArvoreLetras();
        return letras.descodificarPalavra(palavra, letras);
    }

    //ex5 encontrar sequencia comum entre dois simbolos a e b
    public String sequenciaComum(String a, String b) {
        String morseA, morseB;
        morseA = arvore.descodificarPalavra(a, this.arvore);
        morseB = arvore.descodificarPalavra(b, this.arvore);
        Morse m1 = new Morse(morseA, " ", " ");
        Morse m2 = new Morse(morseB, " ", " ");
        return arvore.getLowestCommonAncestor(m1, m2).getRepresentacaoMorse();
    }

    //ex6 devolver uma lista ordenada de consoante o num de ocorrencias pela classe de simbolo
    public void filtrarEOrdenar(List<String> codigosMorse) {
        Map<Integer, String> aux1 = arvore.filtrarEOrdenar(codigosMorse, "Letter");
        System.out.println("Letter:");
        for (Integer i : aux1.keySet()) {
            System.out.println(aux1.get(i));
        }
        System.out.println("_________________________");
        Map<Integer, String> aux2 = arvore.filtrarEOrdenar(codigosMorse, "Number");
        System.out.println("Number:");
        for (Integer i : aux2.keySet()) {
            System.out.println(aux2.get(i));
        }
        System.out.println("_________________________");
        Map<Integer, String> aux3 = arvore.filtrarEOrdenar(codigosMorse, "Punctuation");
        System.out.println("Punctuation:");
        for (Integer i : aux3.keySet()) {
            System.out.println(aux3.get(i));
        }
        System.out.println("_________________________");
        Map<Integer, String> aux4 = arvore.filtrarEOrdenar(codigosMorse, "Non-English");
        System.out.println("Non-English:");
        for (Integer i : aux4.keySet()) {
            System.out.println(aux4.get(i));
        }
    }
}
