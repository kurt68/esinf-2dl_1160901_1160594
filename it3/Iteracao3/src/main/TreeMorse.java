/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import utils.BST;

/**
 *
 * @author Dell M-4700
 */
public class TreeMorse extends BST<Morse> {

    public TreeMorse() {

    }

    //ex 2 
    public String descodificarMorse(String morse) {
        Morse aux = new Morse(morse, " ", " ");
        return this.find(aux, root()).getElement().getRepresentacaoAlfanumerica();
    }
    
     public String descodificarPalavrasMorse(String palavra) {
        String ret = "";

        for (String s : palavra.trim().split(" ")) {
            ret += descodificarMorse(s);
        }
        return ret;
    }

    //ex3 
    public TreeMorse criarArvoreLetras(String classeSimbolo) {
        TreeMorse arvoreLetras = new TreeMorse();
        Morse start = new Morse("r", "start", "root");
        arvoreLetras.insert(start);
        if (!classeSimbolo.isEmpty()) {
            criarArvoreLetras(classeSimbolo, this.root(), arvoreLetras);
            return arvoreLetras;
        }
        return null;
    }

    //ex3 metodo recursivo que insere elementos só daquela classeSimbolo numa arvore nova
    public void criarArvoreLetras(String classeSimbolo, Node<Morse> node, TreeMorse nova) {
        if (node == null) {
            return;
        }
        if (node.getElement().getClasseDeSimbolo().equalsIgnoreCase(classeSimbolo)) {
            nova.insert(node.getElement());
            criarArvoreLetras(classeSimbolo, node.getRight(), nova);
            criarArvoreLetras(classeSimbolo, node.getLeft(), nova);
        } else {
            criarArvoreLetras(classeSimbolo, node.getRight(), nova);
            criarArvoreLetras(classeSimbolo, node.getLeft(), nova);
        }
    }

    //ex4 descodificar palavras para codigo morse
    public String descodificarPalavra(String palavra, TreeMorse arvore) {

        if (palavra.length() == 1) {
            return arvore.getMorseByLetter(arvore.root(), palavra).getRepresentacaoMorse();
        }

        String ret = " ";
        int limit = palavra.length();
        int i = 0;
        while (i < limit) {
            String aux = palavra.substring(i, i + 1);
            ret += arvore.getMorseByLetter(arvore.root(), aux).getRepresentacaoMorse() + " ";
            i++;
        }
        return ret;
    }

    //ex4 metodo para descobrir o objeto Morse, dando a sua letra
    public Morse getMorseByLetter(Node<Morse> current, String ltr) {
        Morse result = null;
        if (current == null) {
            return null;
        }
        if (current.getElement().getRepresentacaoAlfanumerica() != null) {
            if (current.getElement().getRepresentacaoAlfanumerica().equalsIgnoreCase(ltr)) {
                return current.getElement();
            }
        }
        if (current.getLeft() != null) {
            result = getMorseByLetter(current.getLeft(), ltr);
        }
        if (result == null) {
            result = getMorseByLetter(current.getRight(), ltr);
        }
        return result;
    }

    //ex5 a última vez que 2 nós foram iguais foi no último pai em comum
    public Morse getLowestCommonAncestor(Morse p1, Morse p2) {
        return getLCA(root, p1, p2);
    }

    //ex5 lowest common ancestor recursivo
    private Morse getLCA(Node<Morse> node, Morse m1, Morse m2) {
        //se a node atual dor null quer dizer que nao tem um morse comum entre eles
        if (node == null) {
            return null;
        }

        if ((node.getElement().compareTo(m1) < 0 && node.getElement().compareTo(m2) > 0) || (node.getElement().compareTo(m1) > 0 && node.getElement().compareTo(m2) < 0)) {
            return node.getElement();
        }

        Morse leftLCA = getLCA(node.getLeft(), m1, m2);
        Morse rightLCA = getLCA(node.getRight(), m1, m2);

        if (leftLCA != null && rightLCA != null) {
            return node.getElement();
        }

        return (leftLCA != null) ? leftLCA : rightLCA;
    }

    //ex 6 devolve um mapa com a ordenação para a classe de simbolo correspondente
    public Map<Integer, String> filtrarEOrdenar(List<String> codigosMorse, String classeSimbolo) {

        if (classeSimbolo.isEmpty() || codigosMorse.isEmpty()) {
            return null;
        }
        Comparator c = (Comparator<Integer>) (Integer o1, Integer o2) -> o2.compareTo(o1);
        Map<Integer, String> ret = new TreeMap<>(c);
        Morse aux, obj;
        int cont;
        for (String palavra : codigosMorse) {
            cont = 0;
            for (String morse : palavra.trim().split(" ")) {
                aux = new Morse(morse, " ", " ");
                obj = this.find(aux, this.root()).getElement();
                if (obj.getClasseDeSimbolo().equalsIgnoreCase(classeSimbolo)) {
                    cont++;
                }
            }
            if (cont != 0) {
                ret.put(cont, this.descodificarPalavrasMorse(palavra));
            }
        } 
        return ret;
    }
}
