/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Objects;

/**
 *
 * @author Dell M-4700
 */
public class Morse implements Comparable<Morse> {

    private String representacaoMorse;
    private String representacaoAlfanumerica;
    private String classeDeSimbolo;

    private static final String representacaoMorse_OMISSAO = " ";
    private static final String representacaoAlfanumerica_OMISSAO = " ";
    private static final String classeDeSimbolo_OMISSAO = " ";

    public Morse(String representacaoMorse, String representacaoAlfanumerica, String classeDeSimbolo) {
        this.representacaoMorse = representacaoMorse;
        this.representacaoAlfanumerica = representacaoAlfanumerica;
        this.classeDeSimbolo = classeDeSimbolo;
    }

    public Morse() {
        representacaoMorse = representacaoMorse_OMISSAO;
        representacaoAlfanumerica = representacaoAlfanumerica_OMISSAO;
        classeDeSimbolo = classeDeSimbolo_OMISSAO;
    }

    public String getRepresentacaoMorse() {
        return representacaoMorse;
    }

    public void setRepresentacaoMorse(String representacaoMorse) {
        this.representacaoMorse = representacaoMorse;
    }

    public String getRepresentacaoAlfanumerica() {
        return representacaoAlfanumerica;
    }

    public void setRepresentacaoAlfanumerica(String representacaoAlfanumerica) {
        this.representacaoAlfanumerica = representacaoAlfanumerica;
    }

    public String getClasseDeSimbolo() {
        return classeDeSimbolo;
    }

    public void setClasseDeSimbolo(String classeDeSimbolo) {
        this.classeDeSimbolo = classeDeSimbolo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 43 * hash + Objects.hashCode(this.representacaoMorse);
        hash = 43 * hash + Objects.hashCode(this.representacaoAlfanumerica);
        hash = 43 * hash + Objects.hashCode(this.classeDeSimbolo);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Morse other = (Morse) obj;
        if (!Objects.equals(this.representacaoMorse, other.representacaoMorse)) {
            return false;
        }
        if (!Objects.equals(this.representacaoAlfanumerica, other.representacaoAlfanumerica)) {
            return false;
        }
        if (!Objects.equals(this.classeDeSimbolo, other.classeDeSimbolo)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return representacaoAlfanumerica;
    }

    @Override
    public int compareTo(Morse o) {

        CharacterIterator atual = new StringCharacterIterator(this.representacaoMorse);
        CharacterIterator novo = new StringCharacterIterator(o.getRepresentacaoMorse());

        if (atual.current() == 'r') {
            if (novo.current() == '.') {
                return 1;
            } else {
                return -1;
            }
        }

        while (atual.current() != '\uFFFF' && novo.current() != '\uFFFF') {
            if (atual.current() != novo.current()) {
                if (novo.current() == '.') {
                    return 1;
                } else {
                    return -1;
                }
            }
            if (atual.next() != novo.next()) {
                if (novo.current() == '.') {
                    return 1;
                } else {
                    return -1;
                }
            }
            atual.next();
            novo.next();
        }

        if (atual.current() == '\uFFFF') {
            if (novo.current() == atual.current()) {
                return 0;
            }
            if (novo.current() == '.') {
                return 1;
            } else {
                return -1;
            }
        }

        if (novo.next() == '.') {
            return 1;
        } else {
            return -1;
        }
    }
}
