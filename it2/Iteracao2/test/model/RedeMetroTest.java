/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.Import;
import utils.graphbase.Graph;

/**
 *
 * @author Dell M-4700
 */
public class RedeMetroTest {

    RedeMetro instance;
    LinkedList<Estacao> caminho = new LinkedList<>();
    LinkedList<Estacao> caminho2 = new LinkedList<>();
    LinkedList<Estacao> caminho3 = new LinkedList<>();
    LinkedList<Estacao> caminho6 = new LinkedList<>();
    Estacao e1, e2, e3, e4, e5, e6, e7, e8, e9, e10, e11, e12, e13, e14, e15,
            e16, e17, e18, e19, e20, e21, e22, e23, e24, e25;

    public RedeMetroTest() throws FileNotFoundException {
        instance = new RedeMetro();
        e1 = Import.getEstacaoByNome(instance.getEstacoes(), "Alma Marceau");
        e2 = Import.getEstacaoByNome(instance.getEstacoes(), "Iena");
        e3 = Import.getEstacaoByNome(instance.getEstacoes(), "Trocadero");
        e4 = Import.getEstacaoByNome(instance.getEstacoes(), "Passy");
        e5 = Import.getEstacaoByNome(instance.getEstacoes(), "Liberte");
        e6 = Import.getEstacaoByNome(instance.getEstacoes(), "Porte de Charenton");
        e7 = Import.getEstacaoByNome(instance.getEstacoes(), "Porte Doree");
        e8 = Import.getEstacaoByNome(instance.getEstacoes(), "Michel Bizot");
        e9 = Import.getEstacaoByNome(instance.getEstacoes(), "Daumesnil");
        e10 = Import.getEstacaoByNome(instance.getEstacoes(), "Dugommier");
        e11 = Import.getEstacaoByNome(instance.getEstacoes(), "Bercy");
        e12 = Import.getEstacaoByNome(instance.getEstacoes(), "Simplon");
        e13 = Import.getEstacaoByNome(instance.getEstacoes(), "Chateau Rouge");
        e14 = Import.getEstacaoByNome(instance.getEstacoes(), "Jules Joffrin");
        e15 = Import.getEstacaoByNome(instance.getEstacoes(), "Anvers");
        e16 = Import.getEstacaoByNome(instance.getEstacoes(), "Marcadet Poissonniers");
        e17 = Import.getEstacaoByNome(instance.getEstacoes(), "Barbes Rochechouart");
        e18 = Import.getEstacaoByNome(instance.getEstacoes(), "Gambetta");
        e19 = Import.getEstacaoByNome(instance.getEstacoes(), "Pelleport");
        e20 = Import.getEstacaoByNome(instance.getEstacoes(), "Saint Fargeau");
        e21 = Import.getEstacaoByNome(instance.getEstacoes(), "Porte des Lilas");
        e22 = Import.getEstacaoByNome(instance.getEstacoes(), "Telegraphe");
        e23 = Import.getEstacaoByNome(instance.getEstacoes(), "Place des Fetes");
        e24 = Import.getEstacaoByNome(instance.getEstacoes(), "Jourdain");
        e25 = Import.getEstacaoByNome(instance.getEstacoes(), "Pyrenees");

        caminho.add(e1);
        caminho.add(e2);
        caminho.add(e3);
        caminho.add(e4);

        caminho2.add(e5);
        caminho2.add(e6);
        caminho2.add(e7);
        caminho2.add(e8);
        caminho2.add(e9);
        caminho2.add(e10);
        caminho2.add(e11);

        caminho3.add(e12);
        caminho3.add(e16);
        caminho3.add(e14);
        caminho3.add(e16);
        caminho3.add(e13);
        caminho3.add(e17);
        caminho3.add(e15);
        caminho3.add(e17);
        caminho3.add(e13);

        caminho6.add(e18);
        caminho6.add(e19);
        caminho6.add(e20);
        caminho6.add(e21);
        caminho6.add(e22);
        caminho6.add(e23);
        caminho6.add(e24);
        caminho6.add(e25);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of conexo method, of class RedeMetro.
     */
    @Test
    public void testConexo() {
        System.out.println("conexo");
        Graph<Estacao, Ligacao> expResult = null;
        Graph<Estacao, Ligacao> result = instance.conexo();
        assertEquals(expResult, result);
    }

    /**
     * Test of caminhoMinEstacoes method, of class RedeMetro, ex4.
     */
    @Test
    public void testCaminhoMinEstacoes() {
        System.out.println("caminhoMinEstacoes");
        double instante = 0;
        Percurso expResult = new Percurso(caminho2, instance.getRedeMetro(), instante);
        Percurso result = instance.caminhoMinimoTempo(e5, e11, instante);
        assertEquals(expResult, result);
    }

    /**
     * Test of caminhoMinimoTempo method, of class RedeMetro, ex5.
     */
    @Test
    public void testCaminhoMinimoTempo() {
        System.out.println("caminhoMinimoTempo");
        double instante = 0;
        Percurso expResult = new Percurso(caminho, instance.getRedeMetro(), instante);
        Percurso result = instance.caminhoMinimoTempo(e1, e4, instante);
        assertEquals(expResult, result);
    }

    /**
     * Test of caminhoMudancasLinha method, of class RedeMetro, ex6.
     */
    @Test
    public void testCaminhoMudancasLinha() {
        System.out.println("caminhoMudancasLinha");
        Percurso expResult = new Percurso(caminho6, instance.getRedeMetro());
        Percurso result = instance.caminhoMudancasLinha(e18, e25);
        assertEquals(expResult, result);
    }

    /**
     * Test of caminhoMinTempoComEstacoes method, of class RedeMetro, ex7.
     */
    @Test
    public void testCaminhoMinTempoComEstacoes() {
        System.out.println("caminhoMinTempoComEstacoes");
        LinkedList<Estacao> passagens = new LinkedList<>();
        passagens.add(e14);
        passagens.add(e15);
        double instante = 1;
        Percurso expResult = new Percurso(caminho3, instance.getRedeMetro(), instante);
        Percurso result = instance.caminhoMinTempoComEstacoes(e12, e13, instante, passagens);
        assertEquals(expResult, result);
    }
}
