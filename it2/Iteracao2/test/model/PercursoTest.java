/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import utils.Import;
import utils.graphbase.Graph;

/**
 *
 * @author Dell M-4700
 */
public class PercursoTest {

    Percurso instance;
    RedeMetro redeMetro;
    LinkedList<Estacao> caminho = new LinkedList<>();
    Estacao e1 = Import.getEstacaoByNome(Import.lerEstacoes(), "Alma Marceau");
    Estacao e2 = Import.getEstacaoByNome(Import.lerEstacoes(), "Iena");
    Estacao e3 = Import.getEstacaoByNome(Import.lerEstacoes(), "Trocadero");
    Estacao e4 = Import.getEstacaoByNome(Import.lerEstacoes(), "Passy");

    public PercursoTest() throws FileNotFoundException {
        redeMetro = new RedeMetro();
        caminho.add(e1);
        caminho.add(e2);
        caminho.add(e3);
        caminho.add(e4);
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of setEstacoesPercorridasPorLinha method, of class Percurso.
     */
    @Test
    public void testSetEstacoesPercorridasPorLinha() {
        System.out.println("setEstacoesPercorridasPorLinha");
        instance = new Percurso(caminho, redeMetro.getRedeMetro());
        Map<String, Set<Estacao>> result = instance.getEstacoesPercorridasPorLinha();
        Map<String, Set<Estacao>> expResult = new HashMap<>();
        Set<Estacao> set1 = new HashSet<>();
        set1.add(e1);
        set1.add(e2);
        set1.add(e3);
        set1.add(e4);
        expResult.put("6", set1);
        Set<Estacao> set2 = new HashSet<>();
        set2.add(e1);
        set2.add(e2);
        set2.add(e3);
        set2.add(e4);
        expResult.put("9", set2);
        assertEquals(expResult, result);
    }

    /**
     * Test of setEstacoesComInstante method, of class Percurso.
     */
    @Test
    public void testSetEstacoesComInstante() {
        System.out.println("setEstacoesComInstante");
        double instante = 12;
        Set<Double> auxSet1 = new HashSet<>();
        Set<Double> auxSet2 = new HashSet<>();
        Set<Double> auxSet3 = new HashSet<>();
        Set<Double> auxSet4 = new HashSet<>();
        instance = new Percurso(caminho, redeMetro.getRedeMetro(), instante);
        instance.setEstacoesComInstante(caminho, redeMetro.getRedeMetro(), instante);
        System.out.println(instance.getEstacoesComInstante());
        Map<Estacao, Set<Double>> result = instance.getEstacoesComInstante();
        Map<Estacao, Set<Double>> expResult = new HashMap<>();
        auxSet1.add(instante);
        expResult.put(e1, auxSet1);
        auxSet2.add(instante + 1);
        expResult.put(e2, auxSet2);
        auxSet3.add(instante + 2);
        expResult.put(e3, auxSet3);
        auxSet4.add(instante + 3);
        expResult.put(e4, auxSet4);
        System.out.println(expResult);
        assertEquals(expResult, result);
    }
}
