/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;
import model.Estacao;
import model.Ligacao;

/**
 *
 * @author Joao Areias
 */
public class Import {

    public static Set<Estacao> lerEstacoes() throws FileNotFoundException {
        Scanner ler = new Scanner(new File("coordinates.csv"));
        Set<Estacao> estacoes = new HashSet<>();
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            String[] aux = linha.trim().split(";");
            Estacao e = new Estacao(aux[0], Float.parseFloat(aux[1]), Float.parseFloat(aux[2]));
            estacoes.add(e);
        }
        ler.close();
        return estacoes;
    }

    public static void lerLinhas(Set<Estacao> estacoes) throws FileNotFoundException {
        Scanner ler = new Scanner(new File("lines_and_stations.csv"));
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            String[] aux = linha.trim().split(";");
            for (Estacao e : estacoes) {
                if (e.getNome().equals(aux[1])) {
                    e.setLinhas(aux[0]);
                }
            }
        }
        ler.close();
    }

    public static Set<Ligacao> lerLigacoes(Set<Estacao> estacoes) throws FileNotFoundException {
        Scanner ler = new Scanner(new File("connections.csv"));
        Set<Ligacao> ligacoes = new HashSet<>();
        Estacao e1, e2;
        while (ler.hasNext()) {
            String linha = ler.nextLine();
            String[] aux = linha.trim().split(";");
            e1 = getEstacaoByNome(estacoes, aux[1]);
            e2 = getEstacaoByNome(estacoes, aux[2]);
            if (e1 != null && e2 != null) {
                Ligacao lig = new Ligacao(aux[0], e1, e2, Double.parseDouble(aux[3]));
                ligacoes.add(lig);
            }
        }
        ler.close();
        return ligacoes;
    }

    public static Estacao getEstacaoByNome(Set<Estacao> estacoes, String nome) {
        for (Estacao e : estacoes) {
            if (e.getNome().equals(nome)) {
                return e;
            }
        }
        return null;
    }
}
