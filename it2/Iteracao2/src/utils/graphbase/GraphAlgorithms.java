/*
* A collection of graph algorithms.
 */
package utils.graphbase;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Set;
import model.Ligacao;

/**
 *
 * @author DEI-ESINF
 */
public class GraphAlgorithms {

    /**
     * Performs breadth-first search of a Graph starting in a Vertex
     *
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qbfs a queue with the vertices of breadth-first search
     */
    public static <V, E> LinkedList<V> BreadthFirstSearch(Graph<V, E> g, V vert) {

        if (!g.validVertex(vert)) {
            return null;
        }

        LinkedList<V> qbfs = new LinkedList<>();
        LinkedList<V> qaux = new LinkedList<>();
        boolean[] visited = new boolean[g.numVertices()];  //default initializ.: false

        qbfs.add(vert);
        qaux.add(vert);
        int vKey = g.getKey(vert);
        visited[vKey] = true;

        while (!qaux.isEmpty()) {
            vert = qaux.remove();
            for (Edge<V, E> edge : g.outgoingEdges(vert)) {
                V vAdj = g.opposite(vert, edge);
                vKey = g.getKey(vAdj);
                if (!visited[vKey]) {
                    qbfs.add(vAdj);
                    qaux.add(vAdj);
                    visited[vKey] = true;
                }
            }
        }
        return qbfs;
    }

    /**
     * Performs depth-first search starting in a Vertex
     *
     * @param g Graph instance
     * @param vOrig Vertex of graph g that will be the source of the search
     * @param visited set of discovered vertices
     * @param qdfs queue with vertices of depth-first search
     */
    private static <V, E> void DepthFirstSearch(Graph<V, E> g, V vOrig, boolean[] visited, LinkedList<V> qdfs) {

        visited[g.getKey(vOrig)] = true;

        for (Edge<V, E> ed : g.outgoingEdges(vOrig)) {
            if (visited[g.getKey(ed.getVDest())] == false) {
                qdfs.add(ed.getVDest());
                DepthFirstSearch(g, ed.getVDest(), visited, qdfs);
            }
        }
    }

    /**
     * @param g Graph instance
     * @param vInf information of the Vertex that will be the source of the
     * search
     * @return qdfs a queue with the vertices of depth-first search
     */
    public static <V, E> LinkedList<V> DepthFirstSearch(Graph<V, E> g, V vert) {

        if (g.validVertex(vert)) {
            boolean[] visited = new boolean[g.numVertices()];
            LinkedList<V> qdfs = new LinkedList<>();
            qdfs.add(vert);
            DepthFirstSearch(g, vert, visited, qdfs);
            return qdfs;
        }
        return null;
    }

    /**
     * Returns all paths from vOrig to vDest
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param vDest Vertex that will be the end of the path
     * @param visited set of discovered vertices
     * @param path stack with vertices of the current path (the path is in
     * reverse order)
     * @param paths ArrayList with all the paths (in correct order)
     */
    private static <V, E> void allPaths(Graph<V, E> g, V vOrig, V vDest, boolean[] visited,
            LinkedList<V> path, ArrayList<LinkedList<V>> paths) {

        visited[g.getKey(vOrig)] = true;
        path.add(vOrig);
        for (Edge<V, E> edg : g.outgoingEdges(vOrig)) {

            if (edg.getVDest().equals(vDest)) {
                path.add(vDest);
                paths.add(path);
                path.removeLast();

            } else {
                if (visited[g.getKey(edg.getVDest())] == false) {
                    allPaths(g, edg.getVDest(), vDest, visited, path, paths);
                }

            }

        }

        visited[g.getKey(vOrig)] = false;
        path.removeLast();
    }

    /**
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @return paths ArrayList with all paths from voInf to vdInf
     */
    public static <V, E> ArrayList<LinkedList<V>> allPaths(Graph<V, E> g, V vOrig, V vDest) {

        LinkedList<V> path = new LinkedList<>();
        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        if (g.validVertex(vOrig) && g.validVertex(vDest)) {
            boolean[] visited = new boolean[g.numVertices()];

            allPaths(g, vOrig, vDest, visited, path, paths);
            return paths;
        }
        return paths;
    }

    /**
     * Computes shortest-path distance from a source vertex to all reachable
     * vertices of a graph g with nonnegative edge weights This implementation
     * uses Dijkstra's algorithm
     *
     * @param g Graph instance
     * @param vOrig Vertex that will be the source of the path
     * @param visited set of discovered vertices
     * @param pathKeys minimum path vertices keys
     * @param dist minimum distances
     */
    protected static <V, E> void shortestPathLength(Graph<V, E> g, V vOrig, V[] vertices,
            boolean[] visited, int[] pathKeys, double[] dist) {

        int vkey = g.getKey(vOrig);
        dist[vkey] = 0;

        while (vkey != -1) {
            vOrig = vertices[vkey];
            visited[vkey] = true;

            for (Edge<V, E> edge : g.outgoingEdges(vOrig)) {
                V vAdj = g.opposite(vOrig, edge);
                int vkeyAdj = g.getKey(vAdj);

                if (!visited[vkeyAdj] && dist[vkeyAdj] > dist[vkey] + edge.getWeight()) {
                    dist[vkeyAdj] = dist[vkey] + edge.getWeight();
                    pathKeys[vkeyAdj] = vkey;

                }
            }

            double minDist = Double.MAX_VALUE;
            vkey = -1;

            for (int i = 0; i < g.numVertices(); i++) {

                if (!visited[i] && dist[i] < minDist) {
                    minDist = dist[i];
                    vkey = i;
                }

            }
        }
    }

    /**
     * Extracts from pathKeys the minimum path between voInf and vdInf The path
     * is constructed from the end to the beginning
     *
     * @param g Graph instance
     * @param voInf information of the Vertex origin
     * @param vdInf information of the Vertex destination
     * @param pathkeys minimum path vertices keys
     * @param path stack with the minimum path (correct order)
     */
    protected static <V, E> void getPath(Graph<V, E> g, V vOrig, V vDest, V[] verts, int[] pathKeys, LinkedList<V> path) {

        int vDestID = g.getKey(vDest);
        int prevID = pathKeys[vDestID];
        V prevV = null;
        for (V v : verts) {
            if (g.getKey(v) == prevID) {
                prevV = v;
            }
        }
        path.add(vDest);
        if (!vOrig.equals(vDest)) {
            getPath(g, vOrig, prevV, verts, pathKeys, path);

        }
    }

    //shortest-path between vOrig and vDest
    public static <V, E> double shortestPath(Graph<V, E> g, V vOrig, V vDest, LinkedList<V> shortPath) {

        ArrayList<LinkedList<V>> paths = new ArrayList<>();
        ArrayList<Double> dists = new ArrayList<>();

        shortestPaths(g, vOrig, paths, dists);
        shortPath.clear();
        int i = 0;

        for (LinkedList<V> path : paths) {
            if (!path.isEmpty() && path.getLast().equals(vDest)) {
                shortPath.addAll(path);
                return dists.get(i);
            }
            i++;
        }
        return 0;
    }

    public static <V, E> void shortestPathLinhas(Graph<V, E> g, V orig, V dest, LinkedList<V> caminho) {

        int[] pathKeys = new int[g.numVertices()];
        int[] dist = new int[g.numVertices()];
        V[] verts = g.allkeyVerts();
        LinkedList<V> aux = new LinkedList<>();
        String s[] = new String[g.numVertices()];

        for (V vertice : g.vertices()) {
            pathKeys[g.getKey(vertice)] = -1;
            dist[g.getKey(vertice)] = Integer.MAX_VALUE;
        }
        aux.add(orig);
        dist[g.getKey(orig)] = 0;

        while (!aux.isEmpty()) {
            V novaOrig = aux.removeFirst();

            for (V adjacente : g.adjVertices(novaOrig)) {
                Ligacao l = (Ligacao) g.getEdge(novaOrig, adjacente).getElement();
                if (dist[g.getKey(adjacente)] > dist[g.getKey(novaOrig)]) {
                    if (!l.getLinha().equals(s[g.getKey(novaOrig)])) {
                        dist[g.getKey(adjacente)] = dist[g.getKey(novaOrig)] + 1;
                    } else {
                        dist[g.getKey(adjacente)] = dist[g.getKey(novaOrig)];
                    }
                    s[g.getKey(adjacente)] = l.getLinha();
                    pathKeys[g.getKey(adjacente)] = g.getKey(novaOrig);
                    aux.add(adjacente);

                }
            }
        }
        getPath(g, orig, dest, verts, pathKeys, caminho);
        aux = revPath(caminho);
        caminho.clear();
        caminho.addAll(aux);
    }

    public static <V, E> void shortestPathWithoutWeight(Graph<V, E> g, V orig, V dest, LinkedList<V> caminho) {

        int[] pathKeys = new int[g.numVertices()];
        int[] dist = new int[g.numVertices()];
        V[] verts = g.allkeyVerts();
        LinkedList<V> aux = new LinkedList<>();
        V origem = verts[g.getKey(orig)];

        for (V vertice : g.vertices()) {
            pathKeys[g.getKey(vertice)] = -1;
            dist[g.getKey(vertice)] = Integer.MAX_VALUE;
        }
        aux.add(orig);
        dist[g.getKey(orig)] = 0;

        while (!aux.isEmpty()) {
            orig = aux.removeFirst();

            for (V adjacente : g.adjVertices(orig)) {
                if (dist[g.getKey(adjacente)] == Integer.MAX_VALUE) {
                    dist[g.getKey(adjacente)] = dist[g.getKey(orig)] + 1;
                    pathKeys[g.getKey(adjacente)] = g.getKey(orig);
                    aux.add(adjacente);
                }
            }
        }
        getPath(g, origem, dest, verts, pathKeys, caminho);
        aux = revPath(caminho);
        caminho.clear();
        caminho.addAll(aux);
    }

    //shortest-path between voInf and all other
    public static <V, E> boolean shortestPaths(Graph<V, E> g, V vOrig, ArrayList<LinkedList<V>> paths, ArrayList<Double> dists) {

        if (!g.validVertex(vOrig)) {
            return false;
        }

        int nverts = g.numVertices();
        boolean[] visited = new boolean[nverts]; //default value: false
        int[] pathKeys = new int[nverts];
        double[] dist = new double[nverts];
        V[] vertices = g.allkeyVerts();

        for (int i = 0; i < nverts; i++) {
            dist[i] = Double.MAX_VALUE;
            pathKeys[i] = -1;
        }

        shortestPathLength(g, vOrig, vertices, visited, pathKeys, dist);

        dists.clear();
        paths.clear();
        for (int i = 0; i < nverts; i++) {
            paths.add(null);
            dists.add(null);
        }
        for (int i = 0; i < nverts; i++) {
            LinkedList<V> shortPath = new LinkedList<>();
            if (dist[i] != Double.MAX_VALUE) {
                getPath(g, vOrig, vertices[i], vertices, pathKeys, shortPath);
            }
            shortPath = revPath(shortPath);
            paths.set(i, shortPath);
            dists.set(i, dist[i]);
        }
        return true;
    }

    /**
     * Reverses the path
     *
     * @param path stack with path
     */
    private static <V, E> LinkedList<V> revPath(LinkedList<V> path) {

        LinkedList<V> pathcopy = new LinkedList<>(path);
        LinkedList<V> pathrev = new LinkedList<>();

        while (!pathcopy.isEmpty()) {
            pathrev.push(pathcopy.pop());
        }

        return pathrev;
    }
}
