/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Set;
import model.Estacao;
import model.Ligacao;
import model.Percurso;
import model.RedeMetro;
import utils.Import;

/**
 *
 * @author Joao Areias
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.FileNotFoundException
     */
    public static void main(String[] args) throws FileNotFoundException {

        RedeMetro metro = new RedeMetro();
//        if (metro.conexo() == null) {
//            System.out.println("Conexo");
//        }
        LinkedList<Estacao> caminho3 = new LinkedList<>();
        Estacao e12 = Import.getEstacaoByNome(metro.getEstacoes(), "Simplon");
        Estacao e13 = Import.getEstacaoByNome(metro.getEstacoes(), "Chateau Rouge");
        Estacao e14 = Import.getEstacaoByNome(metro.getEstacoes(), "Jules Joffrin");
        Estacao e15 = Import.getEstacaoByNome(metro.getEstacoes(), "Anvers");
        Estacao e16 = Import.getEstacaoByNome(metro.getEstacoes(), "Marcadet Poissonniers");
        Estacao e17 = Import.getEstacaoByNome(metro.getEstacoes(), "Barbes Rochechouart");
        Estacao ls = Import.getEstacaoByNome(metro.getEstacoes(), "Gambetta");
        Estacao am = Import.getEstacaoByNome(metro.getEstacoes(), "Pyrenees");
        caminho3.add(e12);
        caminho3.add(e16);
        caminho3.add(e14);
        caminho3.add(e16);
        caminho3.add(e13);
        caminho3.add(e17);
        caminho3.add(e15);
        caminho3.add(e17);
        caminho3.add(e13);
        System.out.println(metro.getRedeMetro());
        System.out.println("\n\n");
        System.out.println(metro.caminhoMudancasLinha(ls, am));
        System.out.println("\n\n");
        System.out.println(metro.caminhoMinEstacoes(ls, am));
    }

}
