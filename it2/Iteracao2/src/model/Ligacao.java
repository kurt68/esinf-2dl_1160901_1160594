/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.Objects;

/**
 *
 * @author Dell M-4700
 */
public class Ligacao {   
    
    private String linha;
    private Estacao estacao1;
    private Estacao estacao2;
    private double tempo;
    
    public Ligacao(String linha, Estacao estacao1, Estacao estacao2, double tempo) {
        this.linha = linha;
        this.estacao1 = estacao1;
        this.estacao2 = estacao2;
        this.tempo = tempo;
    }
    
    public Ligacao() {
        this.linha = "00";
        this.estacao1 = new Estacao();
        this.estacao2 = new Estacao();
        this.tempo = 0;
    }

    public String getLinha() {
        return linha;
    }

    public void setLinha(String linha) {
        this.linha = linha;
    }

    public Estacao getEstacao1() {
        return estacao1;
    }

    public void setEstacao1(Estacao estacao1) {
        this.estacao1 = estacao1;
    }

    public Estacao getEstacao2() {
        return estacao2;
    }

    public void setEstacao2(Estacao estacao2) {
        this.estacao2 = estacao2;
    }

    public double getTempo() {
        return tempo;
    }

    public void setTempo(double tempo) {
        this.tempo = tempo;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 37 * hash + Objects.hashCode(this.linha);
        hash = 37 * hash + Objects.hashCode(this.estacao1);
        hash = 37 * hash + Objects.hashCode(this.estacao2);
        hash = 37 * hash + (int) (Double.doubleToLongBits(this.tempo) ^ (Double.doubleToLongBits(this.tempo) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Ligacao other = (Ligacao) obj;
        if (Double.doubleToLongBits(this.tempo) != Double.doubleToLongBits(other.tempo)) {
            return false;
        }
        if (!Objects.equals(this.linha, other.linha)) {
            return false;
        }
        if (!Objects.equals(this.estacao1, other.estacao1)) {
            return false;
        }
        if (!Objects.equals(this.estacao2, other.estacao2)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String toString() {
        return "Ligacao{" + "linha=" + linha + ", estacao1=" + estacao1 + ", estacao2=" + estacao2 + ", tempo=" + tempo + '}';
    }  
}
