/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import utils.graphbase.Edge;
import utils.graphbase.Graph;

/**
 *
 * @author Dell M-4700
 */
public class Percurso {

    private LinkedList<Estacao> caminho;
    private Map<String, Set<Estacao>> estacoesPercorridasPorLinha;
    private Map<Estacao, Set<Double>> estacoesComInstante;

    public Percurso(LinkedList<Estacao> caminho, Graph<Estacao, Ligacao> redeMetro) {
        this.caminho = caminho;
        this.estacoesPercorridasPorLinha = new HashMap<>();
        this.estacoesComInstante = new HashMap<>();
        setEstacoesPercorridasPorLinha(caminho, redeMetro);
        setEstacoesComInstante(caminho, redeMetro, 0);
    }

    public Percurso(LinkedList<Estacao> caminho, Graph<Estacao, Ligacao> redeMetro, double instanteInicial) {
        this.caminho = caminho;
        this.estacoesPercorridasPorLinha = new HashMap<>();
        this.estacoesComInstante = new HashMap<>();
        setEstacoesPercorridasPorLinha(caminho, redeMetro);
        setEstacoesComInstante(caminho, redeMetro, instanteInicial);
    }

    public Percurso() {
        caminho = new LinkedList<>();
        estacoesPercorridasPorLinha = new HashMap<>();
        estacoesComInstante = new HashMap<>();
    }

    public void setEstacoesPercorridasPorLinha(LinkedList<Estacao> caminho, Graph<Estacao, Ligacao> redeMetro) {
        Set<Estacao> values = new HashSet<>();
        Set<Estacao> nova = new HashSet<>();
        for (Estacao e : caminho) {
            Iterable<Edge<Estacao, Ligacao>> edges = redeMetro.outgoingEdges(e);
            for (Edge<Estacao, Ligacao> edge : edges) {
                if (caminho.contains(edge.getElement().getEstacao2())) {
                    if (estacoesPercorridasPorLinha.containsKey(edge.getElement().getLinha())) {
                        values = estacoesPercorridasPorLinha.get(edge.getElement().getLinha());
                        values.add(e);
                        estacoesPercorridasPorLinha.put(edge.getElement().getLinha(), values);
                    } else {
                        nova.add(e);
                        estacoesPercorridasPorLinha.put(edge.getElement().getLinha(), nova);
                    }
                }
            }
        }
    }

    public void setEstacoesComInstante(LinkedList<Estacao> caminho, Graph<Estacao, Ligacao> redeMetro, double instanteInicial) {
        double c = instanteInicial;
        LinkedList<Estacao> caminhoAux = new LinkedList<>();
        Set<Double> aux = new HashSet<>();
        caminhoAux.addAll(caminho);

        for (Estacao e : caminho) {
            Set<Double> instantes = new HashSet<>();
            estacoesComInstante.put(e, instantes);
        }
        while (!caminhoAux.isEmpty()) {
            Iterable<Edge<Estacao, Ligacao>> edges = redeMetro.outgoingEdges(caminhoAux.getFirst());
            for (Edge<Estacao, Ligacao> edge : edges) {
                if (caminhoAux.contains(edge.getElement().getEstacao2()) && edge.getElement().getEstacao2().equals(caminhoAux.get(1))) {

                    aux = estacoesComInstante.get(caminhoAux.getFirst());
                    aux.add(c);
                    estacoesComInstante.put(caminhoAux.getFirst(), aux);
                    c += edge.getWeight();
                }
            }
            caminhoAux.removeFirst();
        }
        aux = estacoesComInstante.get(caminho.getLast());
        aux.add(c);
        estacoesComInstante.put(caminho.getLast(), aux);
    }

    public LinkedList<Estacao> getCaminho() {
        return caminho;
    }

    public Set<String> getLinhas() {
        return estacoesPercorridasPorLinha.keySet();
    }

    public Map<String, Set<Estacao>> getEstacoesPercorridasPorLinha() {
        return estacoesPercorridasPorLinha;
    }

    public Map<Estacao, Set<Double>> getEstacoesComInstante() {
        return estacoesComInstante;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 17 * hash + Objects.hashCode(this.caminho);
        hash = 17 * hash + Objects.hashCode(this.estacoesPercorridasPorLinha);
        hash = 17 * hash + Objects.hashCode(this.estacoesComInstante);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Percurso other = (Percurso) obj;
        if (!Objects.equals(this.caminho, other.caminho)) {
            return false;
        }
        if (!Objects.equals(this.estacoesPercorridasPorLinha, other.estacoesPercorridasPorLinha)) {
            return false;
        }
        if (!Objects.equals(this.estacoesComInstante, other.estacoesComInstante)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Percurso{" + "Caminho: " + caminho + "\nLinhas: " + getLinhas() + "\nEstacoesComInstante= " + estacoesComInstante
                + " \n\nEstacoesPercorridasPorLinha=  " + estacoesPercorridasPorLinha + '}';
    }
}
