/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

/**
 *
 * @author Joao Areias
 */
public class Estacao {

    private String nome;
    private float latitude;
    private float longitude;
    private Set<String> linhas;

    private static final String NOME_POR_OMISSAO = "Sem nome";
    private static final float LATITUDE_POR_OMISSAO = 0;
    private static final float LONGITUDE_POR_OMISSAO = 0;

    public Estacao(String nome, float latitude, float longitude) {
        this.nome = nome;
        this.latitude = latitude;
        this.longitude = longitude;
        this.linhas = new HashSet<>();
    }

    public Estacao() {
        this.nome = NOME_POR_OMISSAO;
        this.latitude = LATITUDE_POR_OMISSAO;
        this.longitude = LONGITUDE_POR_OMISSAO;
        this.linhas = new HashSet<>();
    }

    public String getNome() {
        return nome;
    }

    public float getLatitude() {
        return latitude;
    }

    public float getLongitude() {
        return longitude;
    }

    public Set<String> getLinhas() {
        return linhas;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public void setLinhas(String linhaID) {
        this.linhas.add(linhaID);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + Objects.hashCode(this.nome);
        hash = 31 * hash + Float.floatToIntBits(this.latitude);
        hash = 31 * hash + Float.floatToIntBits(this.longitude);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Estacao other = (Estacao) obj;
        if (Float.floatToIntBits(this.latitude) != Float.floatToIntBits(other.latitude)) {
            return false;
        }
        if (Float.floatToIntBits(this.longitude) != Float.floatToIntBits(other.longitude)) {
            return false;
        }
        if (!Objects.equals(this.nome, other.nome)) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Estacao{" + "nome=" + nome + " Linha(s)=" + linhas + '}';
    }
}
