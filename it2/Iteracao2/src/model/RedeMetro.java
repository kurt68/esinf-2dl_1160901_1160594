/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import utils.Import;
import utils.graphbase.Edge;
import utils.graphbase.Graph;
import utils.graphbase.GraphAlgorithms;

/**
 *
 * @author Dell M-4700
 */
public class RedeMetro {

    private Graph<Estacao, Ligacao> redeMetro;
    private Set<Estacao> estacoes;

    //exercicio 1
    public RedeMetro() throws FileNotFoundException {
        this.redeMetro = new Graph<>(true);
        this.estacoes = Import.lerEstacoes();
        Import.lerLinhas(estacoes);
        Set<Ligacao> ligacoes = Import.lerLigacoes(estacoes);
        //criar o grafo com as estacoes como vertices
        for (Estacao e : estacoes) {
            redeMetro.insertVertex(e);
        }
        //criar o grafo com as ligacoes como edges
        for (Ligacao l : ligacoes) {
            redeMetro.insertEdge(l.getEstacao1(), l.getEstacao2(), l, l.getTempo());
        }
    }

    public Graph<Estacao, Ligacao> getRedeMetro() {
        return redeMetro;
    }

    public Set<Estacao> getEstacoes() {
        return estacoes;
    }

    //exercicio 2 
    public Graph<Estacao, Ligacao> conexo() {
        Estacao[] vertice = redeMetro.allkeyVerts();
        for (Estacao e : vertice) {
            LinkedList<Estacao> estacoesPer = GraphAlgorithms.BreadthFirstSearch(redeMetro, e);
            if (estacoesPer.size() != redeMetro.numVertices()) {
                return redeMetro;
            }
        }
        return null;
    }

    //exercicio 4 shortestPath pelo minimo num de estacoes
    public Percurso caminhoMinEstacoes(Estacao origem, Estacao destino) {
        LinkedList<Estacao> caminho = new LinkedList<>();
        GraphAlgorithms.shortestPathWithoutWeight(redeMetro, origem, destino, caminho);
        Percurso p = new Percurso(caminho, redeMetro);
        return p;
    }

    //exercicio 5 shortestPath pelo weight dos edges
    public Percurso caminhoMinimoTempo(Estacao origem, Estacao destino, double instante) {
        LinkedList<Estacao> caminho = new LinkedList<>();
        GraphAlgorithms.shortestPath(redeMetro, origem, destino, caminho);
        Percurso p = new Percurso(caminho, redeMetro, instante);
        return p;
    }

    //Exercicio 6 shortestPath pelo numero de mudanças de linha
    public Percurso caminhoMudancasLinha(Estacao origem, Estacao destino) {
        LinkedList<Estacao> caminho = new LinkedList<>();
        GraphAlgorithms.shortestPathLinhas(redeMetro, origem, destino, caminho);
        Percurso p = new Percurso(caminho, redeMetro);
        return p;
    }

    //exercicio 7 shortestPath pelo weight dos edges com estacoes intermedias
    public Percurso caminhoMinTempoComEstacoes(Estacao origem, Estacao destino, double instante, LinkedList<Estacao> estacoes) {
        LinkedList<Estacao> caminho = new LinkedList<>();
        LinkedList<Estacao> aux = new LinkedList<>();
        LinkedList<Estacao> minimo = new LinkedList<>();
        double menor = 300;
        double length;
        Estacao estacaoInicial = origem;
        Estacao rem = new Estacao();
        while (!estacoes.isEmpty()) {
            for (Estacao e : estacoes) {
                length = GraphAlgorithms.shortestPath(redeMetro, estacaoInicial, e, aux);
                if (length < menor) {
                    minimo.clear();
                    menor = length;
                    minimo.addAll(aux);
                    rem = e;
                }
                aux.clear();
            }
            menor = 300;
            if (!caminho.isEmpty()) {
                caminho.removeLast();
            }
            caminho.addAll(minimo);
            estacaoInicial = rem;
            estacoes.remove(rem);
        }
        aux.clear();
        GraphAlgorithms.shortestPath(redeMetro, estacaoInicial, destino, aux);
        caminho.removeLast();
        caminho.addAll(aux);
        Percurso p = new Percurso(caminho, redeMetro, instante);
        return p;
    }
}
